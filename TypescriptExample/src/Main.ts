/// <reference path="./OtherFileExample.ts"/>
// au dessus ca veut dire: référence le fichier OtherFileExample.ts, pour que je puisse me servir ici de ce qu'il y a dedans.


// toujours mettre tes classes (ou ton code) dans un module pour l'organisation, parceque sinon tu vas confondre avec d'autres librairies.
module MAIN
{
	export class Main
	{
		constructor()
		{
			document.getElementById("hello_world").textContent = "HELLO WORLD!!!" + " " + MAIN.someVar;
		}
	}
	export var main = new Main();
	
}

// on aurait pu juste faire ça ici:
// document.getElementById("hello_world").textContent = "HELLO WORLD!!!" + " " + MAIN.someVar; 
// mais c'est plus rigolo de le mettre dans une classe.